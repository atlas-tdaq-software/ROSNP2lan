// -*- c++ -*-
#ifndef LEVEL1ID_H
#define LEVEL1ID_H
#include <vector>

class Level1Id {
public:
   enum l1IdEnum {L1_MIN_WRAP=0x02000000,
                  L1_MAX_WRAP=0xfe000000,
                  L1_INITIAL=0xffffffff};

   static unsigned int highestValue(std::vector<unsigned int> l1Vec);
};

unsigned int Level1Id::highestValue(std::vector<unsigned int> l1Vec) {
   unsigned int latest=L1_INITIAL;
   unsigned int minL1=L1_INITIAL;
   unsigned int upperMinL1=L1_INITIAL;

   for (auto l1 : l1Vec) {
      if (l1==L1_INITIAL) {
         minL1=L1_INITIAL;
         break;
      }
      if (l1>L1_MAX_WRAP && l1<upperMinL1) {
         upperMinL1=l1;
      }
      if (l1<minL1) {
         minL1=l1;
      }
   }
   if (upperMinL1!=L1_INITIAL && minL1<L1_MIN_WRAP) {
      // At least one channel has not wrapped yet, use the lowest
      // value of those near the wrap point
      latest=upperMinL1;
   }
   else {
      latest=minL1;
   }
   return latest;
}

#endif
