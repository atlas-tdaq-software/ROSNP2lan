/**
 * np2lan  A program to spy on the latest available L1 ID in a RObinNP and send 
 * it to the hltsv.
 **/
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include <map>
#include <vector>

#include <sys/mman.h>
#include <fcntl.h>  
#include <sys/stat.h>        

#include <boost/program_options.hpp>

#include "config/Configuration.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "dal/Component.h"

#include "DFdal/NP2lan.h"

#include "DFdal/ROS.h"
#include "DFdal/ReadoutModule.h"
#include "DFdal/HW_InputChannel.h"

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/FSM/FSMCommands.h"

#include "ROSRobinNP/RobinNPMonitoring.h"

#include "is/infodictionary.h"
#include "is/exceptions.h"
#include "ROSNP2lan/Np2lanInfo.h"
#include "Level1Id.h"

#include "ipc/partition.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/Message.h"
#include "asyncmsg/NameService.h"

namespace po = boost::program_options;
using namespace ROS;


    class XONOFF : public daq::asyncmsg::InputMessage {
    public:

        static const uint32_t ID = 0x00DCDF50;

        uint32_t typeId() const override 
        {
            return ID;
        }

        uint32_t transactionId() const override
        {
            return 0;
        }

        void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
        {
            buffers.push_back(boost::asio::buffer(&m_status, sizeof(m_status)));
        }

        uint32_t status() const
        {
            return m_status;
        }

    private:

        uint32_t m_status;

    };

//====================   STOLEN from LTPMasterTTC2LAN   ====================
/**
 * Message from TTC2LAN to HLTSV.
 *
 * Event update.
 * Payload: a single extendend L1 ID (32bit)
 */
class Update : public daq::asyncmsg::OutputMessage {
public:
  
  // Must be same in HLTSV
  static const uint32_t ID = 0x00DCDF51;
  
  explicit Update(uint32_t l1id)
    : m_l1_id(l1id) {
  }
  
  // Message boiler plate
  uint32_t typeId() const override {
    return ID;
  }
  
  uint32_t transactionId() const override {
    return 0;
  }
  
  void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override {
    buffers.push_back(boost::asio::buffer(&m_l1_id, sizeof(m_l1_id)));
  }
  
private:
  uint32_t m_l1_id;
};
//===========================================================================

/**
 * Class to maintain connection and send data to hltsv
 **/
class OutputSession: public daq::asyncmsg::Session {
public:
   OutputSession(boost::asio::io_service& ioService, uint32_t* xonOffStatus,
                 Np2lanInfo* info);

   OutputSession(const Session&) = delete;
   OutputSession& operator=(const Session& ) = delete;


   ~OutputSession();
   void connect(IPCPartition& ipcPartition,
                std::string& myName);
   void disconnect();
   void send(unsigned int l1Id);

   virtual std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
                                                                      std::uint32_t,
                                                                      std::uint32_t) noexcept{
      if (typeId!=XONOFF::ID) {
         std::cerr << "Invalid message type " << std:: hex << typeId << std::endl;
         return 0;
      }
      return std::unique_ptr<XONOFF>(new XONOFF());
   };
   virtual void onOpen() noexcept {
      asyncReceive();
   };
   virtual void onOpenError(const boost::system::error_code&) noexcept;

   virtual void onClose() noexcept {};
   virtual void onCloseError(const boost::system::error_code&) noexcept{};

   virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message){

      auto msg=std::unique_ptr<XONOFF>(dynamic_cast<XONOFF*>(message.release()));
      if (msg) {
         // std::cout << "Received XONOFF: " << msg->status() << std::endl;
         *m_xonOff=msg->status();
         if (msg->status()) {
            m_info->nXons++;
         }
         else {
            m_info->nXoffs++;
         }
      }
      else {
         std::cout << "Unkown message\n";
      }
      asyncReceive();
   };

   virtual void onReceiveError(const boost::system::error_code&,
                               std::unique_ptr<daq::asyncmsg::InputMessage>) noexcept {
      std::cout << "onReceiveError!!\n";
   };
   virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>) noexcept {};
   virtual void onSendError(const boost::system::error_code&,
                            std::unique_ptr<const daq::asyncmsg::OutputMessage>) noexcept {};

private:
   boost::asio::io_service& m_ioService;
   std::unique_ptr<boost::asio::io_service::work> m_ioServiceWork;
   std::thread m_ioThread;
   uint32_t* m_xonOff;
   Np2lanInfo* m_info;
};


OutputSession::OutputSession(boost::asio::io_service& ioService, uint32_t* xonOffStatus,
   Np2lanInfo* info)
   : daq::asyncmsg::Session(ioService), m_ioService(ioService), m_xonOff(xonOffStatus),
     m_info(info) {
   m_ioServiceWork.reset(new boost::asio::io_service::work(m_ioService));
   m_ioThread=std::thread([&] {m_ioService.run();});
}

OutputSession::~OutputSession(){
   m_ioService.stop();
   m_ioService.reset();
}


void OutputSession::connect(IPCPartition& ipcPartition,
                            std::string& myName) {
   daq::asyncmsg::NameService nameService(ipcPartition,
                                          std::vector<std::string>());
   auto addr=nameService.resolve("TTC2LANReceiver");
   //std::cout << "addr=" << addr << std::endl;
   asyncOpen(myName,addr);
   while(state() != daq::asyncmsg::Session::State::OPEN) {
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
   }
   std::cout << "OutputSession::connect() complete\n";
}

void OutputSession::disconnect(){
   asyncClose();
   while(state() != daq::asyncmsg::Session::State::CLOSED) {
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
   }
   m_ioServiceWork.reset(); // This should tell io_service to stop!
   m_ioThread.join();
}

void OutputSession::send(unsigned int l1Id) {
   asyncSend(std::unique_ptr<Update> (new Update(l1Id)));
}

void OutputSession::onOpenError(const boost::system::error_code& error) noexcept{
  ERS_LOG("Error opening session "
          << error.message() << " (" << error << "). Aborting\n");
}



ERS_DECLARE_ISSUE(ROS,
                  NP2lanIssue,
                  " NP2lan Issue "
                  ERS_EMPTY,
                  ERS_EMPTY
                  )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPShmemException,
                       ROS::NP2lanIssue,
                       " failed to map share memory segment for RobinNP " << addr,
                       ERS_EMPTY,
                       ((unsigned int)addr)
                       )
ERS_DECLARE_ISSUE_BASE(ROS,
                       NPCmdlineException,
                       ROS::NP2lanIssue,
                       " failed to parse command line.",
                       ERS_EMPTY,
                       ERS_EMPTY
                       )
ERS_DECLARE_ISSUE_BASE(ROS,
                       NPPartitionException,
                       ROS::NP2lanIssue,
                       " failed to get partition from DB.",
                       ERS_EMPTY,
                       ERS_EMPTY
                       )
ERS_DECLARE_ISSUE_BASE(ROS,
                       NPConfigurationException,
                       ROS::NP2lanIssue,
                       " failed to find " << name << " in DB.",
                       ERS_EMPTY,
                       ((std::string)name)
                       )
ERS_DECLARE_ISSUE_BASE(ROS,
                       NPModuleException,
                       ROS::NP2lanIssue,
                       "ReadoutApplication " << rosName << " contains relationship to something that is not a " << what,
                       ERS_EMPTY,
                       ((std::string)rosName)
                       ((std::string)what)
                       )
ERS_DECLARE_ISSUE_BASE(ROS,
                       NPChannelException,
                       ROS::NP2lanIssue,
                       "ReadoutModule " << name << " contains relationship to something that is not an InputChannel",
                       ERS_EMPTY,
                       ((std::string)name)
                       )

/**
 * Class to encapsulate access to the shared memory area of one RobinNP
 **/
class RobinNPSharedMem {
public:
   /**
    * Constructor
    *
    * \param card The number of the RobinNP card 0..ncards
    **/
   RobinNPSharedMem (unsigned int card){
      std::ostringstream nameStream;
      nameStream << "/sharedMemRobinNP_" <<std::setw(1) << card;
      //std::cout << nameStream.str() << std::endl;
      auto fd=shm_open(nameStream.str().c_str(),
                       O_RDONLY,
                       S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
      if (fd==-1) {
         perror("Opening shmem file: ");
         throw(NPShmemException(ERS_HERE, card));
      }
      m_monitoringSize = lseek(fd, 0, SEEK_END);

      m_monitoring=(RobinNPMonitoring *) mmap(0, 
                                              m_monitoringSize,
                                              PROT_READ ,
                                              MAP_SHARED,
                                              fd,
                                              0);
      if (m_monitoring==0) {
         throw(NPShmemException(ERS_HERE, card));
      }
      close(fd);
   };

   /**
    * Destructor - Unmap the shared memory
    **/
   ~RobinNPSharedMem() {
      munmap(m_monitoring,m_monitoringSize);
   }

   /**
    * Method to get the address of the memory containing the latest received
    * level1 Id for a given channel
    *
    * \param chan  the number of the channel we're interested in 0..nchans
    **/
   unsigned int* l1IdPointer(unsigned int chan) {
      auto statPtr=m_monitoring->getStatsPtr();
      auto rolStatsPtr=statPtr->getROLStats(chan);
      return &(rolStatsPtr->m_mostRecentId);
   }

   unsigned int* evLogPointer(unsigned int chan) {
      auto statPtr=m_monitoring->getStatsPtr();
      auto rolStatsPtr=statPtr->getROLStats(chan);
      return &(rolStatsPtr->m_eventLog.eventArray[0]);
   }
   int* evLogCurrentIndexPointer(unsigned int chan) {
      auto statPtr=m_monitoring->getStatsPtr();
      auto rolStatsPtr=statPtr->getROLStats(chan);
      return &(rolStatsPtr->m_eventLog.current);
   }
   bool* runningPointer() {
      auto statsPtr=m_monitoring->getStatsPtr();
      return &(statsPtr->m_running);
   }

private:
private:
   RobinNPMonitoring* m_monitoring;
   size_t m_monitoringSize;
};


/**
 * The class that implements the run control FSM for this application
 **/
class Np2lan : public daq::rc::Controllable {
public:
   Np2lan(int argc, char* argv[]);
   ~Np2lan() noexcept;

   void configure(const daq::rc::TransitionCmd&);
   void connect(const daq::rc::TransitionCmd&);
   void disconnect(const daq::rc::TransitionCmd&);
   // void unconfigure(const daq::rc::TransitionCmd&);
   void prepareForRun(const daq::rc::TransitionCmd&);
   void stopHLT(const daq::rc::TransitionCmd&);
   // void user(const daq::rc::UserCmd& command);
   // void publish();
private:
   void run();
   std::string m_executable;
   std::string m_name;
   std::string m_rosName;
   std::map<unsigned int,std::vector<unsigned int> > m_channelList;
   bool m_interactive;
   po::variables_map m_config;
   unsigned int m_pollInterval;
   unsigned int m_updateInterval;
   std::vector<RobinNPSharedMem*> m_sharedMem;
   std::vector<bool*> m_robinRunningPtr;
   std::vector<unsigned int*> m_l1Ptr;
   std::vector<unsigned int*> m_evLogPtr;
   int* m_evLogCurrentIndex;
   std::unique_ptr<std::thread> m_thread;
   bool m_running;
   std::string m_partitionName;
   IPCPartition m_ipcPartition;
   boost::asio::io_service m_ioService;
   std::shared_ptr<OutputSession> m_session;
   std::string m_isInfoName;
   ISInfoDictionary m_dictionary;
   Np2lanInfo m_info;
   uint32_t m_xonOff;
};

Np2lan::Np2lan(int argc, char* argv[])
   :m_executable(argv[0]){
   std::string parent;
   std::string partition;
   std::string segment;
   std::string database;

   po::options_description desc(argv[0]);
   desc.add_options()

      ("name,n", po::value<std::string>(&m_name), "Name of this object in OKS database")
      ("interactive,i",po::bool_switch(&m_interactive), "run from command line without run controller")
      ("partition,p", po::value<std::string>(&partition)->default_value(""), "Name of the partition in OKS database")
      ("segment,s", po::value<std::string>(&segment)->default_value(""), "Name of the segment in OKS database")
      ("parent,P", po::value<std::string>(&parent)->default_value(""), "Name of the parent controller object in OKS database")
      ("database,d", po::value<std::string>(&database)->default_value(""), "Name of the OKS database")
      ;
  
   try {
      po::store(po::parse_command_line(argc, argv, desc), m_config);
      po::notify(m_config);
   }
   catch (std::exception& ex) {
      std::cerr << desc << std::endl;
      ers::fatal(NPCmdlineException(ERS_HERE));
   }
}

Np2lan::~Np2lan(){
   for (auto memIt : m_sharedMem) {
      delete memIt;
   }
}

void Np2lan::configure(const daq::rc::TransitionCmd&){
   // Get config from OKS database
   Configuration confDB("");

   const daq::core::Partition* dbPartition;
   std::string any;
   if(!(dbPartition=daq::core::get_partition(confDB, any))) {
      throw(NPPartitionException(ERS_HERE));
   }

   m_partitionName=dbPartition->UID();
   m_ipcPartition=IPCPartition(m_partitionName.c_str());

   const daq::df::NP2lan* dbApp= confDB.get<daq::df::NP2lan>(m_name);
   if (dbApp==0) {
      throw(NPConfigurationException(ERS_HERE,m_name));
   }



   m_dictionary=ISInfoDictionary(m_ipcPartition);
   m_isInfoName=dbApp->get_ISServer()+".NP2lan."+m_name;


   // Ask that all $variables in the DB are automatically substituted
   confDB.register_converter(
      new daq::core::SubstituteVariables(*dbPartition));

   m_pollInterval=dbApp->get_pollInterval();
   m_updateInterval=dbApp->get_updateInterval();
   const daq::df::ROS* myROS=dbApp->get_spysOnROS();
   if (myROS==0) {
      throw(NPConfigurationException(ERS_HERE,"spysOnROS relationship"));
   }
   m_rosName=myROS->UID();

   // Work out what channels are active in the ROS we're gonna spy on
   std::vector<const daq::core::Component*> disabledComponents=
      dbPartition->get_Disabled();

   for (auto moduleIter : myROS->get_Contains()) {
      const daq::df::ReadoutModule*  roModule=
         confDB.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (moduleIter);
      if (roModule==0 ||
          roModule->class_name()!="RobinNPDescriptorReadoutModule") {
         ers::warning(NPModuleException(ERS_HERE,
                                        m_rosName,
                                        "RobinNPDescriptorReadoutModule"));
         continue;
      }

      auto card=roModule->get_PhysAddress();

      const daq::core::ResourceSet* resources=
         confDB.cast<daq::core::ResourceSet, daq::core::ResourceBase> (moduleIter);
      if (resources==0) {
         ers::warning(NPModuleException(ERS_HERE,m_rosName,"ResourceSet"));
         continue;
      }
      for (auto channelIter : resources->get_Contains()) {
         const daq::df::HW_InputChannel* channelPtr=
            confDB.cast<daq::df::HW_InputChannel, daq::core::ResourceBase> (channelIter);
         if (channelPtr==0) {
            ers::warning(NPChannelException(ERS_HERE,roModule->UID()));
            continue;
         }
         else {
            if (!channelIter->disabled(*dbPartition)) {
               unsigned int channel=channelPtr->get_PhysAddress();
               // std::cout << "Pushing back rol "
               //           << std::hex << channel << std::dec
               //           << " to card " << card
               //           << std::endl;
               m_channelList[card].push_back(channel);
            }
         }
      }
   }
}

void Np2lan::connect(const daq::rc::TransitionCmd&){
   for (auto cardIter : m_channelList) {
      auto card=cardIter.first;
      //std:: cout << "Spying on card " << card << std::endl;
      auto shMem=new RobinNPSharedMem(card);
      m_sharedMem.push_back(shMem);

      m_robinRunningPtr.push_back(shMem->runningPointer());
      bool firstchan=true;
      for (auto channel : cardIter.second) {
         //std::cout << " Adding channel " << std::hex << channel << std::dec << std::endl;
         m_l1Ptr.push_back(shMem->l1IdPointer(channel));
         m_evLogPtr.push_back(shMem->evLogPointer(channel));
         if (firstchan) {
            m_evLogCurrentIndex=shMem->evLogCurrentIndexPointer(channel);
         }
      }


   }
   m_xonOff=1;
   m_info.nXons=0;
   m_info.nXoffs=0;
   m_session=std::make_shared<OutputSession>(m_ioService,&m_xonOff,&m_info);
   m_session->connect(m_ipcPartition,m_name);
}

void Np2lan::disconnect(const daq::rc::TransitionCmd&){
   for (auto memIt : m_sharedMem) {
      delete memIt;
   }
   m_sharedMem.clear();
   m_l1Ptr.clear();

   m_session->disconnect();
   m_session.reset();
}

void Np2lan::prepareForRun(const daq::rc::TransitionCmd&){
   m_running=true;
   m_thread.reset(new std::thread(&Np2lan::run,this));
}

void Np2lan::stopHLT(const daq::rc::TransitionCmd&){
   m_running=false;
   m_thread->join();
   m_thread.reset();
}


void Np2lan::run(){
   enum histEnum{MAX_HISTORY=64};
   unsigned int last=Level1Id::L1_INITIAL;
   unsigned int latest=last;
   int lastEvLogIndex=*m_evLogCurrentIndex;

   m_info.latest=0;
   m_info.lastL1=0;
   m_info.lastECR=0;
   unsigned int nloops=0;
   while (m_running) {
      std::this_thread::sleep_for(std::chrono::microseconds(m_pollInterval));

      bool ready=true;
      for (auto iter : m_robinRunningPtr) {
         ready&=*iter;
      }
      if (!ready) {
         continue;
      }

      m_info.l1Vec.clear();
      for (auto l1Iter : m_l1Ptr) {
         m_info.l1Vec.push_back(*l1Iter);
      }
      latest=Level1Id::highestValue(m_info.l1Vec);
      //////////////////////////////////////////////
      // if (nloops<10) {
      //    std::cout << "Loop " << nloops << std::hex << " last=" << last << ", latest=" << latest << "  xoff=" << m_xonOff << std::dec << std::endl;
      // }
      unsigned int currentEcr=latest>>24; 
      unsigned int lastEcr=last>>24;
      if (currentEcr!=lastEcr) {
         int currentIndex=*m_evLogCurrentIndex;
          // std::cout << "currentIndex=" << currentIndex
          //           << ", lastEvLogIndex=" << lastEvLogIndex
          //           << ", currentEcr=" << currentEcr
          //           << ",lastEcr=" << lastEcr
          //           << std::endl;
         for (int logIndex=lastEvLogIndex;logIndex<currentIndex;logIndex++) {
            unsigned int logValue=m_evLogPtr[0][logIndex];
            //std::cout << std::hex << "Sending m_evLogPtr[0][" << logIndex << "]=" << m_evLogPtr[0][logIndex] <<std::dec << std::endl;
            m_session->send(logValue);
            m_info.l1History.push_back(logValue);
            m_info.lastECR=logValue;
         }
         if (currentIndex<lastEvLogIndex) {
            // std::cout << std::hex << "Sending m_evLogPtr[0][" << lastEvLogIndex << "]=" << m_evLogPtr[0][lastEvLogIndex] <<std::dec << std::endl;
            m_session->send(m_evLogPtr[0][lastEvLogIndex]);
            m_info.l1History.push_back(m_evLogPtr[0][lastEvLogIndex]);
            m_info.lastECR=m_evLogPtr[0][lastEvLogIndex];
            for (int logIndex=0; logIndex<currentIndex;logIndex++) {
               // std::cout << std::hex << "Sending m_evLogPtr[0][" << logIndex << "]=" << m_evLogPtr[0][logIndex] <<std::dec << std::endl;
               m_session->send(m_evLogPtr[0][logIndex]);
               m_info.l1History.push_back(m_evLogPtr[0][logIndex]);
               m_info.lastECR=m_evLogPtr[0][logIndex];
            }
         }
         lastEvLogIndex=currentIndex;
         // std::cout << std::hex << "Sending latest " << latest <<std::dec << std::endl;
         m_session->send(latest);
         m_info.l1History.push_back(latest);
         last=latest;
      }
      if (latest!=last && m_xonOff!=0) {
         last=latest;
         m_session->send(latest);
         m_info.l1History.push_back(latest);
      }
      if (nloops%m_updateInterval==0) {
         m_info.latest=latest;
         m_info.lastL1=last;
         if (m_info.l1History.size()>MAX_HISTORY) {
            auto nclear=m_info.l1History.size()-MAX_HISTORY;
            m_info.l1History.erase(m_info.l1History.begin(),
                                   m_info.l1History.begin()+nclear);
         }
         try {
            m_dictionary.update(m_isInfoName,m_info);
         }
         catch (daq::is::InfoNotFound& exc1) {
            try {
               m_dictionary.insert(m_isInfoName,m_info);
            }
            catch (daq::is::Exception& exc) {
               // Issue a warning and carry on
               ers::warning(exc);
            }
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
         // std::cout << std::dec << std::endl;
      }
      nloops++;
   }
   std::cout << "End of run loop " << nloops << std::hex << " last=" << last << ", latest=" << latest << std::dec << std::endl;
}

int main(int argc, char* argv[]){
   std::shared_ptr<Np2lan> theApp=std::make_shared<Np2lan>(argc,argv);
   daq::rc::CmdLineParser parser(argc,argv,false);
   daq::rc::ItemCtrl myItem(parser, theApp); 
   myItem.init();
   myItem.run();
}
